$(document).ready(onReady);

/*  
*   hides all elements at the begining, so that the user cant press anything before its loaded
*   makes the ajax call to retrieve the needed JSON data
*   calls the changeYear function to reveal the buttons to choose from which year you want the data
*/

function onReady()
{
    $('#section1').hide();
    $('#section2').hide();
    $('#section3').hide();    

    
    $.ajax({
        url: "https://opendata.cbs.nl/ODataApi/odata/37478eng/TypedDataSet",
        dataType: "json",
        success: function(results)
        {
            console.log(results);
            $('#loading').slideUp();
            $('#section1').slideDown();   
            changeYear(results);
        },
        error: function(requestObject,error,errorThrown)
        {
            console.log('error:' + error);
        }

    }).done(function(data) {
        $('#results').append(JSON.stringify(data))
    });
}

/*
*   makes said buttons clickable 
*   calls the dynText function so that the dynamic numbers are shown
*   calls the monthly graphs function so the graphs get revealed and are drawn
*   @param {results} gets the results from the ajax call to put it through to the dynText and monthlyGraph functions
*/
function changeYear(results)
{
    var year = "2015";
    $('#button2015').click(function()
    {
        $('#section2').slideDown();
        document.getElementById('h2Left').innerHTML = 'Total number of passengers <br> that flew in 2015';
        document.getElementById('h2Center').innerHTML = 'Total amount of cargo that was <br>transported in 2015 in tons';
        document.getElementById('h2Right').innerHTML = 'Total amount of mail that was <br>transported in 2015 in tons';
        year = "2015";
        dynText(results,year);
        monthlyGraphs(results,year);
    });
    $('#button2016').click(function()
    {
        $('#section2').slideDown();
        document.getElementById('h2Left').innerHTML = 'Total number of passengers <br> that flew in 2016';
        document.getElementById('h2Center').innerHTML = 'Total amount of cargo that was <br>transported in 2016 in tons';
        document.getElementById('h2Right').innerHTML = 'Total amount of mail that was <br>transported in 2016 in tons';
        year = "2016";
        dynText(results,year);
        monthlyGraphs(results,year);
    });

    
}

/*
*   reveals the dynamic text section and calculates the overall figures for passengers,cargo and mail
*   creates a little count up animation to make it more dynamic
*   @param {results,year} results is used to caluclate the figures, year is used to determine from which year the data is used
*/

function dynText(results,year)
{
    $('#section3').slideDown();
    var totalPass = 0;
    var totalCargo = 0;
    var totalMail = 0;
    
    
    for (var i =0; i<results.value.length;i++)
    {
        //switch to filter the values for the time range between 2010 and 2015
        switch (results.value[i].Periods)
        {
            case (year + "JJ00"): 
            totalPass   = totalPass + results.value[i].TotalPassengers_12;
            totalCargo  = totalCargo + results.value[i].TotalCargo_43;
            totalMail   = totalMail + results.value[i].TotalMail_74;
        }
    }
    var texts = [totalPass,totalCargo, totalMail];

    for(var i=0;i<texts.length;i++)
    {
        $('#dynText' + (i+1)).attr('data-count', texts[i]);
        if ($('#dynText' + (i+1)).attr('data-count') == texts[i])
        {
            $('.counter').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');
        
                $({ countNum: $this.text()}).animate({
                countNum: countTo
                },
        
                {
                duration: 1000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
        
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }
                });  
            });
        }
    }
    
}

/*
*   draws the graphs for the monthly figures
*   uses Chart.js for this (https://cdn.jsdelivr.net/npm/chart.js@2.8.0)
*   @param {results,year} results is used to calculate the monthly figures, year is used to determin from which year the data is used.
*/

function monthlyGraphs(results,year)
{
    //define arrays to store the needed values in
    var passTotal = [];
    var cargoTotal = [];
    var mailTotal = [];

    for(var i=0; i<305; i++)
    {
        if(year == "2015")
        {   
            
            switch (results.value[i].Periods)
            {
                case "2015MM01": 
                passTotal [0] = results.value[i].TotalPassengers_12; 
                cargoTotal[0] = results.value[i].TotalCargo_43; 
                mailTotal[0] = results.value[i].TotalMail_74
                case "2015MM02": 
                passTotal [1] = results.value[i].TotalPassengers_12; 
                cargoTotal[1] = results.value[i].TotalCargo_43; 
                mailTotal[1] = results.value[i].TotalMail_74
                case "2015MM03": 
                passTotal [2] = results.value[i].TotalPassengers_12; 
                cargoTotal[2] = results.value[i].TotalCargo_43; 
                mailTotal[2] = results.value[i].TotalMail_74
                case "2015MM04": 
                passTotal [3] = results.value[i].TotalPassengers_12; 
                cargoTotal[3] = results.value[i].TotalCargo_43; 
                mailTotal[3] = results.value[i].TotalMail_74
                case "2015MM05": 
                passTotal [4] = results.value[i].TotalPassengers_12; 
                cargoTotal[4] = results.value[i].TotalCargo_43; 
                mailTotal[4] = results.value[i].TotalMail_74
                case "2015MM06": 
                passTotal [5] = results.value[i].TotalPassengers_12; 
                cargoTotal[5] = results.value[i].TotalCargo_43; 
                mailTotal[5] = results.value[i].TotalMail_74
                case "2015MM07": 
                passTotal [6] = results.value[i].TotalPassengers_12; 
                cargoTotal[6] = results.value[i].TotalCargo_43; 
                mailTotal[6] = results.value[i].TotalMail_74
                case "2015MM08": 
                passTotal [7] = results.value[i].TotalPassengers_12; 
                cargoTotal[7] = results.value[i].TotalCargo_43; 
                mailTotal[7] = results.value[i].TotalMail_74
                case "2015MM09": 
                passTotal [8] = results.value[i].TotalPassengers_12; 
                cargoTotal[8] = results.value[i].TotalCargo_43; 
                mailTotal[8] = results.value[i].TotalMail_74
                case "2015MM10": 
                passTotal [9] = results.value[i].TotalPassengers_12; 
                cargoTotal[9] = results.value[i].TotalCargo_43; 
                mailTotal[9] = results.value[i].TotalMail_74
                case "2015MM11": 
                passTotal [10] = results.value[i].TotalPassengers_12; 
                cargoTotal[10] = results.value[i].TotalCargo_43; 
                mailTotal[10] = results.value[i].TotalMail_74
                case "2015MM12": 
                passTotal [11] = results.value[i].TotalPassengers_12; 
                cargoTotal[11] = results.value[i].TotalCargo_43; 
                mailTotal[11] = results.value[i].TotalMail_74
            }
        } else if ( year == "2016")
        {
            switch (results.value[i].Periods)
            {
                case "2016MM01": 
                passTotal [0] = results.value[i].TotalPassengers_12; 
                cargoTotal[0] = results.value[i].TotalCargo_43; 
                mailTotal[0] = results.value[i].TotalMail_74
                case "2016MM02": 
                passTotal [1] = results.value[i].TotalPassengers_12; 
                cargoTotal[1] = results.value[i].TotalCargo_43; 
                mailTotal[1] = results.value[i].TotalMail_74
                case "2016MM03": 
                passTotal [2] = results.value[i].TotalPassengers_12; 
                cargoTotal[2] = results.value[i].TotalCargo_43; 
                mailTotal[2] = results.value[i].TotalMail_74
                case "2016MM04": 
                passTotal [3] = results.value[i].TotalPassengers_12; 
                cargoTotal[3] = results.value[i].TotalCargo_43; 
                mailTotal[3] = results.value[i].TotalMail_74
                case "2016MM05": 
                passTotal [4] = results.value[i].TotalPassengers_12; 
                cargoTotal[4] = results.value[i].TotalCargo_43; 
                mailTotal[4] = results.value[i].TotalMail_74
                case "2016MM06": 
                passTotal [5] = results.value[i].TotalPassengers_12; 
                cargoTotal[5] = results.value[i].TotalCargo_43; 
                mailTotal[5] = results.value[i].TotalMail_74
                case "2016MM07": 
                passTotal [6] = results.value[i].TotalPassengers_12; 
                cargoTotal[6] = results.value[i].TotalCargo_43; 
                mailTotal[6] = results.value[i].TotalMail_74
                case "2016MM08": 
                passTotal [7] = results.value[i].TotalPassengers_12; 
                cargoTotal[7] = results.value[i].TotalCargo_43; 
                mailTotal[7] = results.value[i].TotalMail_74
                case "2016MM09": 
                passTotal [8] = results.value[i].TotalPassengers_12; 
                cargoTotal[8] = results.value[i].TotalCargo_43; 
                mailTotal[8] = results.value[i].TotalMail_74
                case "2016MM10": 
                passTotal [9] = results.value[i].TotalPassengers_12; 
                cargoTotal[9] = results.value[i].TotalCargo_43; 
                mailTotal[9] = results.value[i].TotalMail_74
                case "2016MM11": 
                passTotal [10] = results.value[i].TotalPassengers_12; 
                cargoTotal[10] = results.value[i].TotalCargo_43; 
                mailTotal[10] = results.value[i].TotalMail_74
                case "2016MM12": 
                passTotal [11] = results.value[i].TotalPassengers_12; 
                cargoTotal[11] = results.value[i].TotalCargo_43; 
                mailTotal[11] = results.value[i].TotalMail_74
            }
        }
        
    }
    
    var total = [passTotal,cargoTotal,mailTotal];
    var ids = ['passChart', 'cargoChart' , 'mailChart'];
    var labels = ['Monthly figures of Passengers', 'Monthly figure of delivered cargo' , 'Monthly figure of delivered mail']
    for (var i=0; i<total.length;i++)
    {   
        var ctx = document.getElementById(ids[i]).getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
        
            // The data for the dataset
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                datasets: [{
                    label: labels[i],
                    backgroundColor: 'rgb(85, 177, 223)',
                    borderColor: 'rgb(85, 177, 223)',
                    data: total[i]
                }]
            },
        
            // Configuration options could go here
            options: {}
        });   
    }
}
